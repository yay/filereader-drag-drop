String.prototype.interpolate = function(object) {
    var pattern = /(#\{(.*?)\})/g;
    return this.replace(pattern, function() {
        var name = arguments[2];
        return typeof object[name] === 'string' ? object[name] : '';
    });
};

(function () {
    var dropbox = document.getElementById('dropbox'),
        file = document.getElementById('file');

    dropbox.addEventListener('dragenter', dragEnter, false);
    dropbox.addEventListener('dragover', dragOver, false);
    dropbox.addEventListener('dragleave', dragLeave, false);
    dropbox.addEventListener('drop', drop, false);

    file.addEventListener('change', onFileChange, false);

    function dragEnter(event) {
        for (var i = 0, item; item = event.dataTransfer.items[i]; i++) {
            if (!item.type) {
                event.target.className = 'deny';
                return;
            }
        }
        event.target.className = 'hover';
    }

    function dragOver(event) {
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy'; // explicitly show this is a copy
    }

    function dragLeave(event) {
        event.target.className = '';
    }

    function drop(event) {
        event.stopPropagation();
        event.preventDefault();
        dropbox.className = '';

        var files = event.dataTransfer.files; // FileList object
        printSelection(files);
        renderThumbnails(files, '#output');
//        if (files.length) {
//            loadInChunks(files[0]);
//        }
    }

    function onFileChange(event) {
        var files = event.target.files; // FileList object
        // FileList - an array-like sequence of File objects
        // File - an individual file; provides readonly information such as name, file size, mimetype
        printSelection(files);
        renderThumbnails(files, '#output');
    }

    function printSelection(files) {
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            output.push('<li><strong>', f.name, '</strong> (', f.type || 'n/a', ') ',
                f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a', '</li>');
        }
        document.getElementById('output').innerHTML = '<ul id="filelist">' + output.join('') + '</ul>';
    }

    function renderThumbnails(files, container) {
        if (typeof container === 'string') {
            container = document.querySelector(container);
        }
        if (!container) return;
        for (var i = 0, f; f = files[i]; i++) {
            // only process image files
            if (!f.type.match('image.*')) {
                continue;
            }
            var reader = new FileReader();
            // closure to capture the file information
            reader.onload = (function (file) {
                return function (e) {
                    // render thumbnail
                    var img = document.createElement('img');
                    img.className = 'thumb';
                    img.src = e.target.result;
                    img.title = file.name;
                    container && container.insertBefore(img, null);
                }
            })(f);
            // read in the image file as a data URL
            reader.readAsDataURL(f);
        }
    }

    function loadInChunks(file, startByte, stopByte) {
        var start = parseInt(startByte, 10) || 0;
        var stop = parseInt(stopByte, 10) || file.size - 1;
        var reader = new FileReader();

        // if we use onloadend, we need to check the readyState
        reader.onloadend = function (event) {
            if (event.target.readyState === FileReader.DONE) { // DONE == 2
                alert(event.target.result);
            }
        }
        var blob = file.slice(start, stop + 1);
        reader.readAsBinaryString(blob);
    }

})();